
if status is-interactive
    # Commands to run in interactive sessions can go here

#Alias


#Package Manager
alias Mirror="sudo reflector --verbose --latest 15 --sort rate --save /etc/pacman.d/mirrorlist"
alias Keyup="sudo pacman -Sy archlinux-keyring --needed --noconfirm"
alias update="Mirror && Keyup && sudo pacman -Syu --noconfirm && yay -Syu"

alias pacrm="sudo pacman -Rns"
alias dl="sudo pacman -S"
alias pacss="yay -Ss"
alias pacs="pacman -Ss"
alias cleanup="sudo pacman -Rsn (pacman -Qdtq)"
alias pacdbfix="sudo rm /var/lib/pacman/db.lck"

#Qol
alias new="clear && neofetch | lolcat"
alias edit="sudo micro"
alias nano="micro"


#Snapshot management
alias snaprm="sudo snapper rm"
alias snapls="sudo snapper list"
alias snapmk="sudo snapper -v -c root create -t single -d"
alias snapback="sudo snapper rollback"
alias regrub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
# alias snapmount='sudo mount $(df -P / | awk 'NR==2 {print $1}') -o subvolid=256 /MySnap'

#Archive manager
alias tarnow="tar -acf"
alias untar="tar -zxvf"


#File manager
alias ls="eza -al --color=always --group-directories-first"
alias lt="eza -aT --level 2 --long --color=always --group-directories-first"




#Prompt
starship init fish | source

#Pfetch Setup
alias neofetch="pfetch"
alias fetch="pfetch"
alias fastfetch="pfetch"
set -gx PF_SOURCE "$HOME/.config/pfetch.conf"
pfetch | lolcat

# Command helper
thefuck --alias | source

end

set -gx fish_greeting
set -gx WINIT_X11_SCALE_FACTOR 1
