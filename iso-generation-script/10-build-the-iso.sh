#!/bin/bash

# Based on a script made by Erik Dubois

echo "################################################################## "
tput setaf 2
echo "Phase 1 : "
echo "- Verifying Pre-Requisite"
tput sgr0
echo "################################################################## "

	# setting of the general parameters
	archisoRequiredVersion="archiso 76-1"
	buildFolder=$HOME"/KAB-build"
	outFolder=$HOME"/KAB-Iso-Out"
	archisoVersion=$(sudo pacman -Q archiso)

	echo "################################################################## "
	echo "Do you have the right archiso version? : "$archisoVersion
	echo "What is the required archiso version?  : "$archisoRequiredVersion
	echo "Build folder                           : "$buildFolder
	echo "Out folder                             : "$outFolder
	echo "################################################################## "

	if [ "$archisoVersion" == "$archisoRequiredVersion" ]; then
		tput setaf 2
		echo "##################################################################"
		echo "Archiso has the correct version. Continuing ..."
		echo "##################################################################"
		tput sgr0
	else
		tput setaf 1
		echo "###################################################################################################"
		echo "You need to install the correct version of Archiso"
		echo "Use 'sudo downgrade archiso' to do that"
		echo "or update your system"
		echo "If a new archiso package comes in and you want to test if you can still build"
		echo "the iso then change the version in line 28."
		echo "###################################################################################################"
		tput sgr0
	fi



echo
echo "################################################################## "
tput setaf 2
echo "Phase 2 :"
echo "- Deleting the build folder if one exists"
echo "- Copying the Archiso folder to build folder"
tput sgr0
echo "################################################################## "
echo

	echo "Deleting the build folder if one exists - takes some time"
	[ -d $buildFolder ] && sudo rm -rf $buildFolder
	echo
	echo "Copying the Archiso folder to build work"
	echo
	mkdir $buildFolder
	cp -r ../archiso $buildFolder/archiso




echo
echo "################################################################## "
tput setaf 2
echo "Phase 3 :"
echo "- Building the iso - this can take a while - be patient"
tput sgr0
echo "################################################################## "
echo

	[ -d $outFolder ] || mkdir $outFolder
	cd $buildFolder/archiso/
	sudo mkarchiso -v -w $buildFolder -o $outFolder $buildFolder/archiso/



 	echo "Moving pkglist.x86_64.txt"
 	echo "########################"
	rename=$(date +%Y-%m-%d)
 	cp $buildFolder/iso/arch/pkglist.x86_64.txt  $outFolder/KAB-pkglist.txt


echo
echo "##################################################################"
tput setaf 2
echo "DONE"
echo "- Check your out folder :"$outFolder
tput sgr0
echo "################################################################## "
echo
